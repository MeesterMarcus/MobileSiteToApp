import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{



  constructor(public navCtrl: NavController, private iab: InAppBrowser) {

  }

  ngOnInit() {
    const options: InAppBrowserOptions = {
      location : 'no',
      zoom: 'no'
    }
    this.iab.create('https://shoppinglist.google.com', '_self', options);

  }

  

}
